import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import {AuthProvider} from "../../providers/auth/auth";
import {TOKEN} from "../../utils/constants";
import {Storage} from "@ionic/storage";

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  constructor(
    public navCtrl: NavController,
    private auth: AuthProvider,
    private storage: Storage
    ) {
  }

  ionViewCanEnter() {
    return !!this.auth.token;
  }

  logout() {
    this.storage.remove(TOKEN)
      .then(()=>{
        this.auth.token = null;
        this.navCtrl.setRoot('LoginPage')
      })
  }

}
