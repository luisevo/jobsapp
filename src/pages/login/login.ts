import { Component } from '@angular/core';
import {AlertController, IonicPage, Loading, LoadingController, NavController} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthProvider} from "../../providers/auth/auth";
import {Storage} from "@ionic/storage";
import {TOKEN} from "../../utils/constants";

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  form: FormGroup;
  loading: Loading;

  constructor(
    public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public auth: AuthProvider,
    public storage: Storage,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController
  ) {
    this.form = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  ionViewCanEnter() {
    return !this.auth.token;
  }

  ionViewDidLoad() {}

  signIn() {
    this.loading = this.loadingCtrl.create();
    this.loading.present();
    this.auth.signIn(this.form.value)
      .subscribe(
        this.onSuccess,
        this.onError,
        this.onComplete
        )
  }

  onSuccess = ({token}) => {
    this.auth.token = token;
    this.storage.set(TOKEN, token).then(this.goHome)
  };

  onError = (error:any) => {
    this.loading.dismiss();
    this.showAlert('Error', error.error.non_field_errors)
  };

  onComplete = () => {
    this.loading.dismiss();
  };

  goHome = () => {
    this.navCtrl.setRoot('HomePage')
  };

  showAlert(title, message) {
    this.alertCtrl.create({
      title,
      message,
      buttons:[
        {
          text: 'Ok',
          handler: () => {}
        }
      ]
    }).present()
  }
}
