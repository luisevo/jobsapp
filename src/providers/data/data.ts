import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable} from "rxjs";

@Injectable()
export class DataProvider {

  constructor(public http: HttpClient) {
  }

  getJobs(): Observable<any> {
    return this.http.get('/jobs/')
  }

  getProfile(): Observable<any> {
    return this.http.get('/profile/')
  }

  updateProfile(id, body): Observable<any> {
    return this.http.put(`/profile/${id}/`, body)
  }
}
