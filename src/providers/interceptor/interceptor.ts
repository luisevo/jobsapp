import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {AuthProvider} from "../auth/auth";
import {Observable} from "rxjs";
import {BASE_URL} from "../../utils/constants";

@Injectable()
export class InterceptService  implements HttpInterceptor {

  constructor(private auth: AuthProvider) { }

  intercept(request: HttpRequest<any>, next: HttpHandler):Observable<HttpEvent<any>> {
    request = request.clone({
      url: `${BASE_URL}${request.url}`,
      setHeaders: {
        Authorization: `Bearer ${this.auth.token}`
      }
    });
    return next.handle(request)
  };


}
