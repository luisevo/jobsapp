import {Component, ViewChild} from '@angular/core';
import {Nav, Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {Storage} from "@ionic/storage";
import {TOKEN} from "../utils/constants";
import {AuthProvider} from "../providers/auth/auth";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private storage: Storage,
    private auth: AuthProvider
    ) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
      this.validateSession();
    });
  }

  validateSession() {
    this.storage.get(TOKEN)
      .then((token)=>{
        this.auth.token = token;
        if (token) this.nav.setRoot('HomePage');
        else this.nav.setRoot('LoginPage');
      })
  }
}

