export class Profile {
  id: number;
  name: string;
  lastname: string;
  avatar: string;
  age: Date;
}
